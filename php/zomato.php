<?php


include 'zomatoapi/src/ZomatoApi.php';
include 'zomatoapi/src/ZomatoApiException.php';
include 'zomatoapi/src/ZomatoApiRequest.php';


// GET USER INPUT
$term = trim($_GET['term']);
// $term = 'Th';

$a_json = array();
$a_json_row = array();

$a_json_invalid = array(array("id" => "#", "value" => $term, "label" => "Only letters and digits are permitted..."));
$json_invalid = json_encode($a_json_invalid);

// replace multiple spaces with one
$term = preg_replace('/\s+/', ' ', $term);

// SECURITY HOLE ***************************************************************
// allow space, any unicode letter and digit, underscore and dash
if(preg_match("/[^\040\pL\pN_-]/u", $term)) {
  print $json_invalid;
  exit;
}
// *****************************************************************************



// Set up a new instance of the API binding with JSON output
$zomatoapi = new ZomatoApi('89cd2222e168f77b33ed95b0c2922388', 'json');

// Get the city details for London, UK
$restaurants = $zomatoapi->query('search', array('entity_id' => '1', 'entity_type'=>'city', 'collection_id'=>'1'));

$restaurants_json = json_decode($restaurants);
$restaurants = $restaurants_json->restaurants;


$trending_restaurant = array();
foreach ($restaurants as $restaurant) {
    foreach ($restaurant as $key => $value) {
        // echo $value->name."<br>";
        array_push($trending_restaurant, $value->name);
    }
}

// var_dump($trending_restaurant);





function filter_array($main_array, $term){
$filtered_array=array();
 foreach($main_array as $key=>$val){
  if(preg_match("/$term/", $val)){
   $filtered_array[]=$val;
  }
 }
return $filtered_array;
}


$filtered_restaurants=filter_array($trending_restaurant, $term);

print json_encode($filtered_restaurants);




 ?>
