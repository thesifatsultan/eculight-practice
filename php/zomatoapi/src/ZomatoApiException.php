<?php

/**
 * Class ZomatoApiException
 * @package RubenArakelyan\ZomatoApi
 */
class ZomatoApiException extends \Exception {}
