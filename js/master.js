$(document).ready(function() {


    // **************
    // Styling Widget
    // **************
    $('.widget').css("margin-top","40px");

    // *************
    // CHECKBOXRADIO
    // *************

    var el = $('.jquery-ui-widget');

    var checkboxradio = $('.checkboxradio');
    el.append(checkboxradio);

    var title = $('<h4>Checkbox Radio</h4>');
    checkboxradio.append(title);

    var fieldset = $('<fieldset></fieldset>');
    checkboxradio.append(fieldset);

    //Create first label
    var label1 = $("<label for='radio-1'>Working</label>")
    //Create second radio element
    var input1 = $('<input type="radio" name="radio" id="radio-1">')
    //Append input to label
    input1.appendTo(label1);

    //Create second label
    var label2 = $("<label for='radio-2'>Not Working</label>")
    //Create second radio element
    var input2 = $('<input type="radio" name="radio" id="radio-2">')
    //Append input to label
    input2.appendTo(label2);

    //Insert the label into the DOM - replace body with the required position
    fieldset.append(label1);
    fieldset.append(label2);

    //initiating checkboxradio
    $("input").checkboxradio();

    // *****************
    // CHECKBOXRADIO END
    // *****************

    // ************
    // Autocomplete
    // ************

    var availableTags = [
        "ActionScript",
        "AppleScript",
        "Asp",
        "BASIC",
        "C",
        "C++",
        "Clojure",
        "COBOL",
        "ColdFusion",
        "Erlang",
        "Fortran",
        "Groovy",
        "Haskell",
        "Java",
        "JavaScript",
        "Lisp",
        "Perl",
        "PHP",
        "Python",
        "Ruby",
        "Scala",
        "Scheme"
    ];

    var autocomplete = $('.autocomplete');
    el.append(autocomplete);

    var autocomplete_title = $('<h4>Autocomplete</h4>');
    autocomplete.append(autocomplete_title);
    var label_autocomplete = $('<label for="tags">Tags</label>');
    var input_autocomplete = $('<input id="tags">')

    input_autocomplete.appendTo(label_autocomplete);
    autocomplete.append(input_autocomplete);


    // $('#tags').autocomplete({source:availableTags});
      $('#tags').autocomplete({
        source: "/practice/php/zomato.php"
      })

    // ************
    // Autocomplete
    // ************






});



function hello() {
    console.log("hello");
    alert("hello");
}
